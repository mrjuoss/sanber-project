<?php

Route::namespace('Auth')->prefix('auth')->group(function () {
    Route::post('register', 'RegisterController');
    Route::patch('verification/{user}', 'AuthController@verification');
    Route::patch('regenerate-otp/{user}', 'AuthController@regenareteOtp');
    Route::patch('update-password/{user}', 'AuthController@updatePassword');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController');
});

Route::namespace('Profile')->middleware('auth:api')->group(function () {
    Route::get('get-profile', 'ProfileController@index');
    Route::patch('update-profile/{user}', 'ProfileController@updateProfile');
});

Route::get('user', 'UserController');
Route::get('articles/', 'Article\ArticleController@index');
Route::get('articles/{article}', 'Article\ArticleController@show');


Route::group([
    'middleware' => 'api',
    'prefix' => 'blog',
], function () {
    Route::get('random/{count}', 'BlogController@random');
    Route::post('store', 'BlogController@store');
});
