<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
// Proteksi Method atau Controle dg Middleware di Route
// Route::get('/test', 'TestController@test')->middleware('DateMiddleware');

// Route Group
Route::middleware('DateMiddleware')->group(function () {
    Route::get('/test', 'TestController@test');
    Route::get('/test1', 'TestController@test1');
});
Route::get('/test2', 'TestController@test2');
Route::get('/test-admin', 'TestController@testAdmin');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/testUser', 'TestController@testUser');
Route::get('/testRole', 'TestController@testRole');
Route::get('/testOtp', 'TestController@testOtp');

Route::get('/route-1', 'TestController@testRoute1')->middleware(['auth', 'email_verified']);
Route::get('/route-2', 'TestController@testRoute2')->middleware(['auth', 'email_verified','admin']);

Route::get('/test', function () {
    dd(DB::table('users')->get());
});

Route::view('/{any?}', 'app')->where('any', '.*');
