<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Registered User Notification</title>
</head>
<body>
    <p>Selamat Bapak/Ibu {{ $name }} telah berhasil mendaftar pada Aplikasi Sanber Project by Sanber Code.</p>
    <p>OTP Code anda adalah {{ $otp }} </p>
</body>
</html>