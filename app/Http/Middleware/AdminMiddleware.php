<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Role;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();

        $admin_id = Role::where('name', 'Admin')->first();

        if ($user->role_id == $admin_id->id) {
            return $next($request);
        }

        // abort(403);
        return response()->json(['message' => 'Anda bukan Admin']);
    }
}
