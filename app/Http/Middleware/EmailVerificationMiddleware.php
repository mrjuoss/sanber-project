<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\Auth;


use Closure;

class EmailVerificationMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        if (! $user->email_verified_at == null) {
            return $next($request);
        }

        // abort(403);
        return response()->json([
            'message' =>'Email Anda belum terverifikasi'
        ]);
    }
}
