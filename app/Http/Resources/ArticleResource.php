<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'title' => $this->title,
            'body' => $this->body,
            // 'subject' => $this->subject,
            'subject' => $this->subject->name,
            // 'user' => $this->user,
            'author' => $this->user->name,
            // 'publish' => $this->created_at->format("d F Y")
            'publish' => $this->created_at->diffForHumans()
        ];
    }

    public function with($request)
    {
        return ['status' => 'success'];
    }
}
