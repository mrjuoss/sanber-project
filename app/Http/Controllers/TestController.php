<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Auth;

class TestController extends Controller
{
    // Proteksi Controller dg Middleware
    // public function __construct()
    // {
    //     $this->middleware('DateMiddleware');
    // }

    public function test()
    {
        // Simple case:
        // Route ini hanya akan bisa diakses pada tanggak 1 s.d 3
        // $current_date = Carbon::now()->day;
        // dd($current_date);
        // if ($current_date >= 1 && $current_date <= 3) {
        //     return "Berhasil masuk";
        // }
        // return "Gagal masuk";
        return "Berhasil masuk";
    }

    public function test1()
    {
        return "Berhasil masuk ke halama test 1";
    }
    public function test2()
    {
        return "Halaman ini bisa diakses setiap saat";
    }

    public function testAdmin()
    {
        return "Admin berhasil masuk";
    }

    public function testUser()
    {
        dd(Auth::user());
    }
    public function testRole()
    {
        dd(Auth::user()->role()->name());
    }
    public function testOtp()
    {
        dd(Auth::user()->otp()->name);
    }

    public function testRoute1()
    {
        return "Berhasil masuk Route 1";
    }

    public function testRoute2()
    {
        return "Berhasil masuk Route 2";
    }
}
