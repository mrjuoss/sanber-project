<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\RegisterRequest;
use App\OtpCode;
use App\User;
use Carbon\Carbon;
use App\Events\UserRegisteredEvent;


class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
       $user = User::create([
            'name' => request('name'),
            'username' => request('username'),
            'email' => request('email'),
            'password' => bcrypt(
                request('password')
            ),
            'role_id' => 'fd7cbc8c-2f3e-4418-97a0-b12bf4817696'
        ]);

       $now = Carbon::now();

       $otp = OtpCode::create([
            'user_id' => $user->id,
            'otp' => random_int(100, 999),
            'valid_until' => $now->addMinutes(5),
       ]);

       // $user = User::where('name', request('name'))->first();

       // $user->otpCode()->save($otp);
       
       // Saat User berhasil Register, kirimkan Notofikasi via Email
       // Cara 1 (Tanpa Menggunakan Event Listener)
       // Mail::to($user)->send(new UserRegisteredMail($user)); 
       // 
       // Cara 2 (Menggunakan Event Listener tanpa Queue)
       event(new UserRegisteredEvent($user));

       return response()->json([
            'response_code'=> '00',
            'response'=> 'Register user berhasil. Silahkan check email anda',
            'data'=> $user
       ], 200);
    }
}
