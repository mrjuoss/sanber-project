<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\OtpCode;
use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function regenareteOtp(Request $request, User $user)
    {
        $user = User::where('email', request('email'))->first();
        // return $user;
        $now = Carbon::now();
        $otp = $user->otpCode->update([
            'otp' => random_int(100, 999),
            'valid_until' => $now->addMinutes(5),
        ]);
      
        return response()->json([
            'response_code'=> '00',
            'response_message'=> 'OTP berhasil digenerate. Silahkan check',
        ]);
    }

    public function verification(Request $request, User $user)
    {
        $otp = $user->OtpCode()->otp;

        if (!$otp)
        {
            return response()->json([
                'response_code'=> '01',
                'response_message'=> 'OTP Code tidak ditemukan'
            ]);
        }

        $now = Carbon::now();

        if ($now > $otp->valid_until)
        {
            return response()->json([
                'response_code'=> '01',
                'response_message'=> 'OTP Code sudah tidak berlaku. Silahkan generate ulang!'
            ]);
        }

        $user->update([
            'email_verified_at' => NOW()
            ]);

            return response()->json([
                'response_code'=> '00',
                'response_message'=> 'Verifkasi berhasil',
                'data'=> ['user' => $user]
            ]);

        
    }

    public function updatePassword(Request $request, User $user)
    {
        $user->update([
            'password' => bcrypt($request->password),
            'updated_at' => NOW()
        ]);

        return response()->json([
            'response_code'=> '00',
            'response_message'=> 'Pasword berhasil diubah',
            'data'=> $user
        ]);
    }
}
