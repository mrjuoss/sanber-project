<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        request()->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        if (!$token = auth()->attempt($request->only('email', 'password')))
        {
            return response()->json([
                'response_code' => "01",
                'response_message' => "Silahkan check kembali username dan password anda"
            ]);
        }

        $user = auth()->user();

        if ($user->email_verified_at == null)
        {
            return response()->json([
                'response_code' => "01",
                'response_message' => "Silahkan verifikasi email anda"
            ]);
        }

        return response()->json([
            'response_code' => "00",
            'response_message' => "User berhasil login",
            'data' => [
                'token' => $token,
                'user' => $user
            ]
        ]);
    }
}
