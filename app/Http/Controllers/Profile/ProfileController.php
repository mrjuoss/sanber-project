<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index(User $user)
    {
        return response()->json([
            'response_code' => '00',
            'response_message' => 'Profile berhasil didapatkan',
            'data' => ['profile' => $user]
        ]);
    }
    public function updateProfile(Request $request, User $user)
    {

        $file = $request->file('photo');
        $fileName = 'upload/user/'.time()."_".$file->getClientOriginalName();
        $destinationUpload = 'upload/user ';
        $file->move($destinationUpload,$fileName);


        $user = User::firstWhere('id',$request->user()->id);
        $user->name = $request->name;
        $user->photo = $fileName;
        $user->updated_at = NOW();
        $user->update();

        return response()->json([
            'response_code' => '00',
            'response_message' => 'Profile berhasil diupdate',
            'data' => ['profile' => $user]
        ]);
    }

}
