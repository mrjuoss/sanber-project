<?php

namespace App\Models\Article;

use App\Traits\UsesUuid;
use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    use UsesUuid;

    protected $fillable = ['name', 'slug'];

    public function articles()
    {
        return $this->hasMany(Article::class);
    }
}
