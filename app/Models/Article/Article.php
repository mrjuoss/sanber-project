<?php

namespace App\Models\Article;

use App\Traits\UsesUuid;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use UsesUuid;
    
    protected $fillable = ['title', 'slug', 'body', 'user_id', 'subject_id'];

    // Eager Loading
    protected $with = ['subject', 'user'];

    public function getRouteKeyName()
    {
        return "slug";
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }
}
