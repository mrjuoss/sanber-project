<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOtpCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('otp_codes', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->integer('otp');
            $table->datetime('valid_until');
            // Field user_id karena 1 user hanya mempunyai 1 otp dan 1 otp untuk 1 user
            $table->uuid('user_id');
            // on Delete Cascade -> Jika PK yg terkait dg FK ini dihapus, maka row FK dalam tabel ini akan ikut kehapus
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('otp_codes');
    }
}
