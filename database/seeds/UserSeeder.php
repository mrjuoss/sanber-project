<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->name = "Mujaki";
        $user->username = "mrjuoss";
        $user->email = "mrjuoss@gmail.com";
        $user->password = bcrypt("12345678");
        $user->role_id = "b4727fbd-801b-4e62-a637-7d1bc19aef4b";
        $user->save();
    }
}
