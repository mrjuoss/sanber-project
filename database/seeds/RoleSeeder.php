<?php

use Illuminate\Database\Seeder;
use App\Role;
use Illuminate\Support\Str;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $admin = new Role();
        // $admin->name = "Administrator";
        // $admin->save();

        Role::insert([
            'id' => Str::Uuid(),
            'name' => 'User'
        ]);
    }
}
