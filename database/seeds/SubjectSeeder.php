<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SubjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subjects')->insert([
            'id' => Str::uuid(),
            'name' => "Laravel",
            'slug' => "laravel"
        ]);

        DB::table('subjects')->insert([
            'id' => Str::uuid(),
            'name' => "PHP",
            'slug' => "php"
        ]);
    }
}
